import './App.sass';
import Home from './feature/home/presentation/home_page'
import WeaponPlan from './feature/weapon-plan/presentation/weapon_plan_page'
import CarData from './feature/car-data/presentation/car_data_page'
import Thanks from './feature/thanks/presentation/thanks_page'

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
    <Router>
      <Switch>
        <Route path= "/" exact>
          <Home/>
        </Route>
        <Route exact path= "/datos-auto">
          <CarData />
        </Route>
        <Route exact path= "/armar-plan/">
          <WeaponPlan />
        </Route>
        <Route exact path= "/gracias">
          <Thanks />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
