import React, {useEffect, useState} from 'react'
import './style/home_page.sass'
import Banner from './components/banner'
import BannerMovil from './components/banner_movil'
import LocalPhoneIcon from '@material-ui/icons/LocalPhone';
import { useForm } from 'react-hook-form'
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link
  } from "react-router-dom";

import {
	Box,
	Button,
	Checkbox,
	FormControlLabel,
	Grid,
	TextField,
} from '@material-ui/core'


var evalua:boolean = false;

const HomePage =() => {
	let us ={
		title :'',
		first :'',
		last :'',
	}
	const [state, setState] = useState({
		checkedA: evalua,
	});
	const [ use, setEquipo] = useState(null);

	useEffect(()=>{
		obtenerDatos()
	}, [])

	const obtenerDatos = async() =>{
		const data = await fetch('https://randomuser.me/api/', );
		const user = await data.json()
		console.log(user['results'][0]['name']);
		us = user['results'][0]['name'];
		console.log(us.title);
		console.log(us.first);
		console.log(us.last);
		setEquipo(null);
		// const users = await data.json(),

	}

	const { register, formState: { errors }, handleSubmit } = useForm();

	const onSubmit = (data:any) =>{
		console.log(data['documento'])
		console.log(data['celular'])
		console.log(data['placa'])
		console.log(evalua);
	}

	const handleChanges = (event:any) => {
		setState({ ...state, [event.target.name]: event.target.checked });
		evalua = state.checkedA;
	};

	return (
		<div>
			<Grid container >
				<Banner />
				<BannerMovil />
				<Grid container item xs={12}  md={7}>
					<Grid item xs={12} className ='d-none d-md-block text-end'>
						<Box pt={2}>
							<h6 >
								¿Tiene alguna duda? <span> <LocalPhoneIcon/> (01)4111 6001</span>
							</h6>
						</Box>
					</Grid>
					<Grid  container justify='center'>
						<Box>
							<h2>Déjanos tus datos</h2>
							<Box width= {400} textAlign='center'>
								<form onSubmit={handleSubmit(onSubmit)}>
									{errors.firstName && "Last name is required"}
									<TextField
										{...register("documento", { required: true, maxLength: 8 })}
										variant="outlined"
										margin="normal"
										required
										fullWidth
										label="Documento"
									/>

									{errors.documento && "revise su dni"}
									<TextField
										{...register("celular", { required: true, maxLength: 9 })}
										variant="outlined"
										margin="normal"
										required
										fullWidth
										label="Celular"

									/>
									{errors.celular && "verifique su numero"}
									<TextField
										{...register("placa", { required: true, maxLength: 6 , pattern: /^(R|S)+[A-Z0-9]{5}$/ }) }
										variant="outlined"
										required
										fullWidth
										label="Placa"
									/>
									{errors.placa && "Placa no valida"}
									<Box>
										<Grid container>
											<Grid item xs={1}>
												<FormControlLabel
													control={<Checkbox value="remember" color="primary" />}
													label=''
													onChange={handleChanges}
													name="checkedA"
												/>
											</Grid>
											<Grid item xs={11} >
												<p>Acepto  <span> la politica de proteccion de Datos  Personales</span> y los <span> terminos y condiciones </span> </p>
											</Grid>
										</Grid>
									</Box>
									<Button className=' btn btp' type="submit">
										COTÍZALO
									</Button>
								</form>

							</Box>
						</Box>
					</Grid>
				</Grid>
			</Grid>
		</div>
	)
}

export default HomePage