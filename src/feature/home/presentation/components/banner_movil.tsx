import { Box, Grid } from "@material-ui/core"
import LocalPhoneIcon from '@material-ui/icons/LocalPhone';
import img  from '../../../../assets/Mask Group.png'
import logo  from '../../../../assets/logo.png'

const BannerMovil =() => {
	return <Grid item xs={12} className = 'd-block d-md-none'>
		<div className='dad'  >
			<img className='childImg' src= { img } alt="" />
			<Box p={2} pt={2}>
				<Grid container >
					<Grid item xs={6}>
						<img  src= { logo } alt=""/>
					</Grid>
					<Grid item xs={6} className =' text-end'>
						<h6>
							<span>
							<LocalPhoneIcon/> Llámanos
							</span>
						</h6>
					</Grid>
				</Grid>
			</Box>
			<Box p={2} pt={9} >
				<Grid container >
					<Grid item xs={12}>
						<p>!Nuevo!</p>
					</Grid>
					<Grid item xs={12}>
						<h1>Seguro Vehicular </h1>
					</Grid>
					<Grid item xs={12}>
						<h2 style={{color: '#EF3340', paddingBottom:'10px'}}
						>Tranking </h2>
					</Grid>
					<Box width={200}>
						<p>Cuentanos donde le haras seguimiento a tu seguro</p>
					</Box>
				</Grid>

			</Box>
		</div>
	</Grid>
}

export default BannerMovil