import { Grid } from "@material-ui/core"
import principal  from '../../../../assets/princpal.png'


const Banner =() => {
	return <Grid item xs={5} className = 'ht d-none d-md-block'>
		<img src= { principal } style={{height: '100%', width: '100%'}} alt="" />
	</Grid>
}

export default Banner